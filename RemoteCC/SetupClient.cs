﻿using System;
using System.Net;
using System.Net.Sockets;

namespace RemoteCC
{
    public static class SetupClient
    {
        public static void Setup(string ip, int port)
        {
            IPAddress ipaddr = IPAddress.Parse(ip);
            IPEndPoint ep = new IPEndPoint(ipaddr, port);

            Socket sock = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            try
            {
                sock.Connect(ep);
            }
            catch (Exception E)
            {
                Console.WriteLine(E.ToString() + " 연결 실패");
            }
            //Console.WriteLine("Success");
            sock.Close();
        }
    }
}
