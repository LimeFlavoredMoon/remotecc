﻿using System;
using System.Drawing;

namespace RemoteCC
{
    public class RecvKMEEventArgs : EventArgs
    {
        public Meta Meta { get; private set; }

        public int Key { get { return Meta.Key; } }
        public Point Now { get { return Meta.Now; } }
        public MsgType MT { get { return Meta.Mt; } }

        internal RecvKMEEventArgs(Meta meta)
        {
            this.Meta = meta;
        }

        public delegate void RecvKMEEventHandler(object sender, RecvKMEEventArgs e);
    }
}
