﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static RemoteCC.RecvKMEEventArgs;

namespace RemoteCC
{
    public partial class VirtualCursorForm : Form
    {
        public VirtualCursorForm()
        {
            InitializeComponent();
        }

        private void VirtualCursorForm_Load(object sender, EventArgs e)
        {
            Remote.Singleton.RecvedKMEvent += new RecvKMEEventHandler
                (Singleton_RecvedKMEvent);
        }

        void Singleton_RecvedKMEvent(object sender, RecvKMEEventArgs e)
        {
            if(e.MT == MsgType.MT_M_MOVE)
            {
                //Location = new Point(e.Now.X + 3, e.Now.Y + 3);
            }
        }
    }
}
