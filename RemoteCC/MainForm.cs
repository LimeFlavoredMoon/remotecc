﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static RemoteCC.RecvRCInfoEventArgs;

namespace RemoteCC
{
    public partial class MainForm : Form
    {

        string sip;
        int sport;
        RemoteClientForm rcf = null;
        VirtualCursorForm vcf = null;
        
        public MainForm()
        {
            InitializeComponent();
            
        }

        private void btn_StartServer_Click(object sender, EventArgs e)
        {
            Rectangle rect = Remote.Singleton.Rect;

            string host_ip = tbox_ip.Text;
            Controller.Singleton.Start(host_ip);
            rcf.ClientSize = new Size(1280, 720);
            rcf.Show();
        }

        private void btn_Connect_Click(object sender, EventArgs e)
        {
            this.Hide();

            Remote.Singleton.RecvEventStart();
            timer_send_img.Start();
            vcf.Show();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            IPHostEntry host_entry = Dns.GetHostEntry(Dns.GetHostName());
            
            //foreach (IPAddress ipaddr in host_entry.AddressList)
            //{
            //    Console.WriteLine(ipaddr);
            //}
            vcf = new VirtualCursorForm();
            rcf = new RemoteClientForm();
            Remote.Singleton.RecvedRCInfo += new RecvRCInfoEventHandler(Remote_RecvedRCInfo); ;
        }

        delegate void Remote_Dele(object sender, RecvRCInfoEventArgs e);
        private void Remote_RecvedRCInfo(object sender, RecvRCInfoEventArgs e)
        {
            if(this.InvokeRequired)
            {
                object[] objs = new object[2] { sender, e };
                this.Invoke(new Remote_Dele(Remote_RecvedRCInfo), objs);
            }
            else
            {
                tbox_controller_ip.Text = e.IPAddressStr;
                sip = e.IPAddressStr;
                sport = e.Port;
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Remote.Singleton.Stop();
            Controller.Singleton.Stop();
            Application.Exit();
        }

        private void noti_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }

        private void timer_send_img_Tick(object sender, EventArgs e)
        {
            Rectangle rect = Remote.Singleton.Rect; //사각 영역 구함
            Bitmap bitmap = new Bitmap(rect.Width, rect.Height); //비트맵 생성
            Graphics gp = Graphics.FromImage(bitmap);//비트맵에서 Graphics 개체 생성

            Size size2 = new Size(rect.Width, rect.Height);

            gp.CopyFromScreen(new Point(0, 0), new Point(0, 0), size2);//화면 복사
            gp.Dispose();
            try
            {
                ImageClient ic = new ImageClient(sip, NetworkInfo.ImgPort);
                ic.SendImageAsync(bitmap, null);//이미지를 비동기로 전송
                
            }
            catch (Exception E)
            {
                timer_send_img.Stop();
                MessageBox.Show("서버에 연결 실패");
                Console.WriteLine(E);
            }
        }
    }
}
