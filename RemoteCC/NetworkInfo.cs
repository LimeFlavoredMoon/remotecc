﻿using System;
using System.Net;
using System.Net.Sockets;
using static RemoteCC.RecvImageEventArgs;

namespace RemoteCC
{
    public static class NetworkInfo
    {
        public static short ImgPort { get { return 20004; } }
        public static short SetupPort { get { return 20002; } }
        public static short EventPort { get { return 20006; } }
        public static string DefaultIP
        {
            get
            {
                string host_name = Dns.GetHostName();

                IPHostEntry host_entry = Dns.GetHostEntry(host_name);

                foreach(IPAddress ipaddr in host_entry.AddressList)
                {
                    if(ipaddr.AddressFamily == AddressFamily.InterNetwork && ipaddr.ToString() != "192.168.56.1")
                    {
                        //Console.WriteLine("MYIP : " + ipaddr.ToString());
                        return ipaddr.ToString();
                    }
                }
                return string.Empty;
            }
        }
    }

    public class Controller
    {
        static Controller singleton;

        public static Controller Singleton { get { return singleton; } }
        static Controller() { singleton = new Controller(); }
        private Controller() { }

        ImageServer img_server = null;
        SendEventClient sce = null;

        public event RecvImageEventHandler RecvedImage = null;

        string host_ip;

        public SendEventClient SendEventClient { get { return sce; } }
        public string MyIP { get { return NetworkInfo.DefaultIP; } }
        public void Start(string host_ip)
        {
            this.host_ip = host_ip;

            img_server = new ImageServer(MyIP, NetworkInfo.ImgPort);

            img_server.RecvedImage += new RecvImageEventHandler(
                img_server_RecvedImage);

            SetupClient.Setup(host_ip, NetworkInfo.SetupPort);
        }

        void img_server_RecvedImage(object sender, RecvImageEventArgs e)
        {
            if(RecvedImage != null)
            {
                RecvedImage(this, e);
            }
        }

        public void StartEventClient()
        {
            sce = new SendEventClient(host_ip, NetworkInfo.EventPort);
        }

        public void Stop()
        {
            if(img_server != null)
            {
                img_server.Close();
                img_server = null;
            }
        }
    }
}
