﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static RemoteCC.RecvImageEventArgs;

namespace RemoteCC
{
    public partial class RemoteClientForm : Form
    {
        bool check;
        Size csize;

        SendEventClient EventSC
        { get { return Controller.Singleton.SendEventClient; } }
        
        public RemoteClientForm()
        {
            InitializeComponent();
        }

        private void pbox_remote_Click(object sender, EventArgs e)
        {

        }

        private void RemoteClientForm_Load(object sender, EventArgs e)
        {
            Controller.Singleton.RecvedImage += new RecvImageEventHandler(
                Singleton_RecvImageEventHandler);
        }

        void Singleton_RecvImageEventHandler(object sender, RecvImageEventArgs e)
        {
            if(!check)
            {
                Controller.Singleton.StartEventClient();
                check = true;
                csize = e.Image.Size;
            }

            pbox_remote.Image = e.Image;
        }

        private void RemoteClientForm_KeyDown(object sender, KeyEventArgs e)
        {
            if(check)
            {
                EventSC.SendKeyDown(e.KeyValue);
            }
        }

        private void RemoteClientForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (check)
            {
                EventSC.SendKeyUp(e.KeyValue);
            }
        }

        private void RemoteClientForm_MouseMove(object sender, MouseEventArgs e)
        {
            if(check)
            {
                Point pt = ConvertPoint(e.X, e.Y);
                EventSC.SendMouseMove(pt.X, pt.Y);
            }
        }

        private Point ConvertPoint(int x, int y)
        {
            int nx = csize.Width * x / pbox_remote.Width;
            int ny = csize.Height * y / pbox_remote.Height;

            return new Point(nx,ny);
        }

        private void RemoteClientForm_MouseDown(object sender, MouseEventArgs e)
        {
            if(check)
            {
                Text = e.Location.ToString();
                EventSC.SendMouseDown(e.Button);
            }
        }

        private void RemoteClientForm_MouseUp(object sender, MouseEventArgs e)
        {
            if(check)
            {
                EventSC.SendMouseUp(e.Button);
            }
        }
    }
}
