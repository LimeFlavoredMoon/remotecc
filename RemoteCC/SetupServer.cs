﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using static RemoteCC.RecvRCInfoEventArgs;

namespace RemoteCC
{
    public static class SetupServer
    {
        static Socket lisn_sock;
        static Thread accept_thread = null;

        static public event RecvRCInfoEventHandler RecvedRCInfo = null;

        static public void Start(string ip, int port)
        {
            IPAddress ipaddr = IPAddress.Parse(ip);
            IPEndPoint ep = new IPEndPoint(ipaddr, port);

            lisn_sock = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            lisn_sock.Bind(ep);
            lisn_sock.Listen(1);

            ThreadStart ts = new ThreadStart(AcceptLoop);
            accept_thread = new Thread(ts);
            accept_thread.Start();
            //Console.WriteLine("Started");
        }

        private static void AcceptLoop()
        {
            try
            {
                while(true)
                {
                    Socket dosock = lisn_sock.Accept();
                    if (RecvedRCInfo != null)
                    {
                        RecvRCInfoEventArgs e = new RecvRCInfoEventArgs(dosock.RemoteEndPoint);

                        RecvedRCInfo(null, e);
                    }
                    dosock.Close();
                }
            }
            catch
            {
                Close();
            }
        }

        public static void Close()
        {
            if(lisn_sock != null)
            {
                lisn_sock.Close();
                lisn_sock = null;
            }
        }
    }
}
