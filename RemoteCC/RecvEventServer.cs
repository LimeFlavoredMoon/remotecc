﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using static RemoteCC.RecvKMEEventArgs;

namespace RemoteCC
{
    public class RecvEventServer
    {
        Socket lisn_sock;

        public event RecvKMEEventHandler RecvedKMEvent = null;

        Thread th;

        public RecvEventServer(string ip, int port)
        {
            lisn_sock = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            IPAddress ipaddr = IPAddress.Parse(ip);
            IPEndPoint ep = new IPEndPoint(ipaddr, port);
            lisn_sock.Bind(ep);
            lisn_sock.Listen(5);

            ThreadStart ts = new ThreadStart(AcceptLoop);

            th = new Thread(ts);
            th.Start();
        }

        public delegate void ReceiveDele(Socket socket);

        private void AcceptLoop()
        {
            Socket dosock;
            ReceiveDele rld = new ReceiveDele(Receive);

            try
            {
                while(true)
                {
                    dosock = lisn_sock.Accept();
                    rld.BeginInvoke(dosock, null, null);
                }
            }
            catch
            {
                Close();
            }

        }

        public void Close()
        {
            if(lisn_sock != null)
            {
                lisn_sock.Close();
                lisn_sock = null;
            }
        }

        void Receive(Socket socket)
        {
            byte[] buffer = new byte[9];
            int n = socket.Receive(buffer);

            if(RecvedKMEvent != null)
            {
                RecvKMEEventArgs e = new RecvKMEEventArgs(new Meta(buffer));
                RecvedKMEvent(this, e);
            }
            socket.Close();
        }
    }
}
