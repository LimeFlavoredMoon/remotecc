﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using static RemoteCC.RecvImageEventArgs;

namespace RemoteCC
{
    public class ImageServer
    {
        Socket lisn_sock;
        Thread accept_thread = null;

        public event RecvImageEventHandler RecvedImage = null;

        public ImageServer(string ip, int port)
        {
            lisn_sock = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream,
                ProtocolType.Tcp);

            IPAddress ipaddr = IPAddress.Parse(ip);
            IPEndPoint ep = new IPEndPoint(ipaddr, port);
            lisn_sock.Bind(ep);

            lisn_sock.Listen(5);
            ThreadStart ts = new ThreadStart(AcceptLoop);

            accept_thread = new Thread(ts);
            accept_thread.Start();
        }

        private void AcceptLoop()
        {
            try
            {
                while(lisn_sock != null)
                {
                    Socket do_sock = lisn_sock.Accept();
                    //Console.WriteLine("Looping");

                    Receive(do_sock);
                    //Console.WriteLine("Received");
                }
            }
            catch
            {
                Console.WriteLine("ImageServerLoopingError");
                Close();
            }
        }

        public void Close()
        {
            if(lisn_sock != null)
            {
                lisn_sock.Close();
                lisn_sock = null;
            }
        }

        private void Receive(Socket do_sock)
        {
            byte[] lbuf = new byte[4];
            do_sock.Receive(lbuf);
            //Console.WriteLine("Length Buffer Received");

            int len = BitConverter.ToInt32(lbuf, 0);
            byte[] buffer = new byte[len];

            int trans = 0;
            while (trans < len)
            {
                trans += do_sock.Receive(buffer, trans,
                    len - trans, SocketFlags.None);
            }
            //Console.WriteLine("Image Buffer Received");

            if (RecvedImage != null)
            {
                IPEndPoint iep = do_sock.RemoteEndPoint as IPEndPoint;
                RecvImageEventArgs e = new RecvImageEventArgs(iep, ConvertBitmap(buffer));
                RecvedImage(this, e);
            }
            do_sock.Close();
        }

        public Bitmap ConvertBitmap(byte[] buffer)
        {
            MemoryStream ms = new MemoryStream();

            ms.Write(buffer, 0, (int)buffer.Length);

            Bitmap bitmap = new Bitmap(ms);
            return bitmap;
        }
    }
}
