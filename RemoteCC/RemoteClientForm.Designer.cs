﻿namespace RemoteCC
{
    partial class RemoteClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbox_remote = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbox_remote)).BeginInit();
            this.SuspendLayout();
            // 
            // pbox_remote
            // 
            this.pbox_remote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbox_remote.Location = new System.Drawing.Point(0, 0);
            this.pbox_remote.Name = "pbox_remote";
            this.pbox_remote.Size = new System.Drawing.Size(744, 667);
            this.pbox_remote.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbox_remote.TabIndex = 0;
            this.pbox_remote.TabStop = false;
            this.pbox_remote.Click += new System.EventHandler(this.pbox_remote_Click);
            this.pbox_remote.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RemoteClientForm_MouseDown);
            this.pbox_remote.MouseMove += new System.Windows.Forms.MouseEventHandler(this.RemoteClientForm_MouseMove);
            this.pbox_remote.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RemoteClientForm_MouseUp);
            // 
            // RemoteClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 667);
            this.Controls.Add(this.pbox_remote);
            this.Name = "RemoteClientForm";
            this.Text = "RemoteClientForm";
            this.Load += new System.EventHandler(this.RemoteClientForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RemoteClientForm_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RemoteClientForm_KeyUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RemoteClientForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.RemoteClientForm_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RemoteClientForm_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.pbox_remote)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbox_remote;
    }
}