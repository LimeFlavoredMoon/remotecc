﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace RemoteCC
{
    public class ImageClient
    {
        Socket sock;
        
        public ImageClient(string ip, int port)
        {
            sock = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream,
                ProtocolType.Tcp);

            IPAddress ipaddr = IPAddress.Parse(ip);
            IPEndPoint ep = new IPEndPoint(ipaddr, port);
            try
            {
                sock.Connect(ep);
            }
            catch (Exception E)
            {
                Console.WriteLine(E + " 연결 실패");
            }
        }

        public bool SendImage(Image img)
        {
            if(sock == null)
            {
                Console.WriteLine("Socket is null !!");
                return false;
            }
            MemoryStream ms = new MemoryStream();

            img.Save(ms, ImageFormat.Jpeg);

            byte[] data = ms.GetBuffer();
            try
            {
                int trans = 0;
                byte[] lbuf = BitConverter.GetBytes(data.Length);
                sock.Send(lbuf);

                while (trans < data.Length)
                {
                    trans += sock.Send(data, trans, data.Length - trans,
                        SocketFlags.None);
                }
                //Console.WriteLine("Buffer Sended");
                sock.Close();
                sock = null;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Application.Exit();
                return false;
            }
        }

        public delegate bool SendImageDele(Image img);
        
        public void SendImageAsync(Image img, AsyncCallback callback)
        {
            SendImageDele dele = new SendImageDele(SendImage);

            dele.BeginInvoke(img, callback, this);
        }

        public void Close()
        {
            if (sock != null)
            {
                sock.Close();
                sock = null;
            }
        }
    }
}
