﻿using System;
using System.Drawing;
using System.Windows.Automation;
using static RemoteCC.RecvKMEEventArgs;
using static RemoteCC.RecvRCInfoEventArgs;

namespace RemoteCC
{
    public class Remote
    {
        static Remote singleton;
        public static Remote Singleton { get { return singleton; } }
        static Remote() { singleton = new Remote(); }
        public event RecvRCInfoEventHandler RecvedRCInfo = null;
        public event RecvKMEEventHandler RecvedKMEvent = null;

        RecvEventServer res = null;

        public Rectangle Rect { get; private set; }
        private Remote()
        {
            //최상위 자동화 요소 구하기
            AutomationElement ae = AutomationElement.RootElement;
            //사각 영역 구하기
            System.Windows.Rect rt = ae.Current.BoundingRectangle;
            //Rectangle 형식으로 변환
            Rect = new Rectangle((int)rt.Left, (int)rt.Top, (int)rt.Width, (int)rt.Height);
            //원격제어 요청 수신 이벤트 메시지 핸들러 등록
            SetupServer.RecvedRCInfo += new RecvRCInfoEventHandler(SetupServer_RecvedRCInfo);
            SetupServer.Start(MyIP, NetworkInfo.SetupPort);//원격 제어 요청 서버 가동
        }

        void SetupServer_RecvedRCInfo(object sender, RecvRCInfoEventArgs e)
        {
            if (RecvedRCInfo != null)
            {
                RecvedRCInfo(this, e);
            }
        }

        public string MyIP { get { return NetworkInfo.DefaultIP; } }

        public void RecvEventStart()
        {
            res = new RecvEventServer(MyIP, NetworkInfo.EventPort);
            res.RecvedKMEvent += new RecvKMEEventHandler(res_RecvKMEEventHandler);
        }

        void res_RecvKMEEventHandler(object sender, RecvKMEEventArgs e)
        {
            if(RecvedKMEvent != null)
            {
                RecvedKMEvent(this, e);
            }

            switch(e.MT)
            {
                case MsgType.MT_KDOWN: WrapNative.KeyDown(e.Key); break;
                case MsgType.MT_KEYUP: WrapNative.KeyUp(e.Key); break;
                case MsgType.MT_M_LEFTDOWN: WrapNative.LeftDown(); break;
                case MsgType.MT_M_LEFTUP: WrapNative.LeftUp(); break;
                case MsgType.MT_M_MOVE: WrapNative.Move(e.Now); break;
            }
        }

        public void Stop()
        {
            SetupServer.Close();

            if (res != null)
            {
                res.Close();
                res = null;
            }
        }
    }
}
